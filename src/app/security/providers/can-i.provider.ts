import {Observable} from 'rxjs';

export abstract class CanIProvider {
  abstract getCans(): Observable<Set<string>>;
}
