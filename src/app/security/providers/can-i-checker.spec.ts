import {TestBed} from '@angular/core/testing';

import {CanIChecker} from './can-i-checker';

describe('CanIChecker', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CanIChecker = TestBed.get(CanIChecker);
    expect(service).toBeTruthy();
  });
});
