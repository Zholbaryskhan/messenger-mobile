import {Injectable} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {CanIProvider} from './can-i.provider';

@Injectable()
export class CanIChecker {

  constructor(protected canIProvider: CanIProvider) {
    console.log('CanIChecker.constructor');
  }

  canI(permission: string | string[]): Observable<boolean> {
    return this.canIProvider.getCans()
      .pipe(
        map((cans: Set<string>) => {
          if (isNullOrUndefined(permission)) {
            return false;
          }

          permission = Array.isArray(permission) ? permission : [permission];
          return permission.some(permitted => cans.has(permitted));
        }),
      );
  }
}
