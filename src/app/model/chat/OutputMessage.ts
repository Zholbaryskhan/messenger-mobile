export interface OutputMessage {
  from: number;
  text: string;
  avatar: string;
  own: boolean;
  time: Date;
}
