export interface ChatList {
  id: number;
  fio: string;
  time: Date;
  lastMessage: string;
  own: boolean;
  src: string;
}
