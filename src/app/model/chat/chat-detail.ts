export interface ChatDetail {
  id: number;
  fio: string;
  channelId: number;
}
