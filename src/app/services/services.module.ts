import {APP_INITIALIZER, ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {CoreModule} from '../core/core.module';
import {IonicStorageModule} from '@ionic/storage';
import {CanIProvider, SecurityModule} from '../security';
import {throwIfAlreadyLoaded} from '../core/module-import-guard';
import {CustomCanIProvider} from './security/custom-can-i-provider';
import {ProbeService} from './probe.service';
import {Push} from '@ionic-native/push/ngx';
import {HttpCancelService} from './http-cancel.service';
import {AppLoaderService} from './app-loader.service';
import {UserService} from './user.service';
import {filterInterceptorRequest, initApp} from '@util/app.factories';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AUTH_TOKEN_INTERCEPTOR_FILTER, AuthModule, OAuth2JWTInterceptor} from '@auth/index';
import {HttpErrorInterceptor} from './interceptors/http-error.interceptor';
import {authOptions} from '@util/app.options';
import {TestService} from './test.service';
import {ChatService} from './chat.service';

const NATIVE_PROVIDERS = [
  Push
];

const PROVIDERS = [
  ProbeService,
  HttpCancelService,
  AppLoaderService,
  UserService,
  TestService,
  ChatService,
  {provide: APP_INITIALIZER, useFactory: initApp, deps: [AppLoaderService], multi: true},
  {provide: CanIProvider, useClass: CustomCanIProvider},
  {provide: HTTP_INTERCEPTORS, useClass: OAuth2JWTInterceptor, multi: true},
  {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true},
  {provide: AUTH_TOKEN_INTERCEPTOR_FILTER, useValue: filterInterceptorRequest}
];

@NgModule({
  declarations: [],
  imports: [
    CoreModule.forRoot(),
    SecurityModule.forRoot(),
    AuthModule.forRoot(authOptions)
  ]
})
export class ServicesModule {
  constructor(@Optional() @SkipSelf() parentModule: ServicesModule) {
    throwIfAlreadyLoaded(parentModule, 'ServicesModule');
  }

  static forRoot(): ModuleWithProviders<ServicesModule> {
    return {
      ngModule: ServicesModule,
      providers: [
        ...PROVIDERS,
        ...NATIVE_PROVIDERS
      ],
    } as ModuleWithProviders<ServicesModule>;
  }
}
