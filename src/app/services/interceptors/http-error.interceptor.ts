import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {HttpCancelService} from '../http-cancel.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private router: Router,
              private httpCancelService: HttpCancelService) {
    // router.events.subscribe(event => {
    //   // An event triggered at the end of the activation part of the Resolve phase of routing.
    //   if (event instanceof ActivationEnd) {
    //     // Cancel pending calls
    //     console.log('ActivationEnd:',)
    //
    //   }
    // });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
        takeUntil(this.httpCancelService.onCancelPendingRequests()),
        catchError((error: HttpErrorResponse) => {

          if (error instanceof HttpErrorResponse
              && error.status === 401
              && error.error.errorCode === 10) {

            console.log('redirect:',);
            this.httpCancelService.cancelPendingRequests();
            this.router.navigate(['/app/login']);
          }
          // this.alertService.error(`${response.status}: ${response.message}`);
          return throwError(error);
        })
    );
  }
}
