import { Injectable } from '@angular/core';
import {ApiService} from '../core/services/api.service';
import {OutputMessage} from '../model/chat/OutputMessage';
import {ChatList} from '../model/chat/chat-list';
import {ChatDetail} from '../model/chat/chat-detail';

@Injectable()
export class ChatService {

  constructor(private api: ApiService) {
    this.api = api.setPrefix('/chat-message-mobile');
  }

  listMessage(channelId): Promise<OutputMessage[]>{
    return this.api.get('/list', {channelId: channelId})
    .toPromise()
    .then(res => res as OutputMessage[]);
  }

  getChatList(): Promise<ChatList[]>{
    return this.api.get('/chat-list')
    .toPromise()
    .then(res => res as ChatList[]);
  }

  getChatDetail(friendId: number): Promise<ChatDetail>{
    return this.api.get('/chat-detail', {friendId: friendId})
    .toPromise()
    .then(res => res as ChatDetail);
  }
}
