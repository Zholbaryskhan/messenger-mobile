import {Injectable} from '@angular/core';
import {ApiService} from '../core/services/api.service';

@Injectable()
export class UserService {

  constructor(private apiService: ApiService) {
    this.apiService = this.apiService.setPrefix('/user');
  }

  loadUserInfo(){
    return this.apiService.get('/info');
  }
}
