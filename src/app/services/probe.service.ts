import {Injectable} from '@angular/core';
import {ApiService} from '../core/services/api.service';

@Injectable()
export class ProbeService {

  constructor(private apiService: ApiService) {
    this.apiService = apiService.setPrefix('/probe');
  }

  greeting() {
    const reqOpts = {
      responseType: 'text'
    };
    return this.apiService.get('/', null, reqOpts)
      .toPromise()
      .then(res => res as string);
  }

  registration(token: string) {

    return this.apiService.get('/registration', {token})
      .toPromise()
      .then(res => res as string);
  }
}
