import {Injectable} from '@angular/core';
import {ApiService} from '../core/services/api.service';

@Injectable()
export class TestService {

  constructor(private api: ApiService) {
    this.api = api.setPrefix('/chat-message-mobile');
  }

  list() {
    return this.api.get('/list', {channelId: 'hey'})
    .toPromise()
    .then(res => res);
  }

}
