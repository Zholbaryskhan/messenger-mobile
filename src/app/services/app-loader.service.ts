import {Injectable} from '@angular/core';
import {UserService} from './user.service';

@Injectable()
export class AppLoaderService {

  constructor(private userService: UserService) {
  }

  initApp() {
    return this.userService.loadUserInfo()
      .toPromise()
      .catch(err => {
        console.error('Произошла ошибка при загрузки данный сессии');
      });
  }
}
