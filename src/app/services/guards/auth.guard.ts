import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {AuthService} from '@auth/services';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

  roles: string[] = ['admin'];

  constructor(protected router: Router, private authService: AuthService) {
  }


  canActivate(route: ActivatedRouteSnapshot) {
    return this.authService.isAuthenticated().pipe(
      map((authenticated) => {
        if (authenticated) { return authenticated; }

        return this.router.createUrlTree(['/auth/login']);
      })
    );

    /*return new Promise(resolve => {
      const requiredRoles = route.data.roles;
      if (!requiredRoles || requiredRoles.length === 0) {
        return resolve(true);
      } else {
        if (!this.roles || this.roles.length === 0) {
          resolve(false);
        }
        let granted = false;
        for (const requiredRole of requiredRoles) {
          if (this.roles.indexOf(requiredRole) > -1) {
            granted = true;
            break;
          }
        }
        resolve(granted);
      }

    });*/
  }

}
