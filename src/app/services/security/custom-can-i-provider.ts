import {Injectable} from '@angular/core';
import {CanIProvider} from '../../security';
import {Observable, of} from 'rxjs';

@Injectable()
export class CustomCanIProvider implements CanIProvider {

  getCans(): Observable<Set<string>> {
    return of(new Set(['asd']));
  }
}
