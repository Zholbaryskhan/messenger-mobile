import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'app/my-friend',
    pathMatch: 'full'
  },
  {
    path: 'app/my-friend',
    loadChildren: '@pages/my-friends/my-friends.module#MyFriendsPageModule'
  }
  ,
  {
    path: 'app/login',
    loadChildren: '@pages/login/login.module#LoginPageModule'
  },
  {
    path: 'app/chat-list',
    loadChildren: '@pages/chat-list/chat-list.module#ChatListPageModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
