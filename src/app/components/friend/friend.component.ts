import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.scss'],
})
export class FriendComponent implements OnInit {

  @Input() fio: string;
  @Input() city: string;
  @Input() src: string;

  @Output() onClickDetail: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  clickDetail(event) {
    this.onClickDetail.emit(event);
  }
}
