import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FriendComponent} from './friend.component';
import {IonicModule} from '@ionic/angular';



@NgModule({
  declarations: [FriendComponent],
  exports: [
    FriendComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class FriendModule { }
