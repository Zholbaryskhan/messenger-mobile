import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ApiService} from './services/api.service';
import {HttpClientModule} from '@angular/common/http';
import {throwIfAlreadyLoaded} from './module-import-guard';

const SERVICES = [
  ApiService,
];

const MODULES = [
  CommonModule,
  HttpClientModule
];

@NgModule({
  declarations: [],
  imports: [
    ...MODULES,

  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        ...SERVICES
      ],
    } as ModuleWithProviders<CoreModule>;
  }

}
