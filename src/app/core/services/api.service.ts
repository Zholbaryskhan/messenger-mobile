import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {isNullOrUndefined} from 'util';

const keyValueAppender = (keyValue: { [p: string]: any }, appendFunc: (key: string, value: string | null) => void) => {
  if (!keyValue) {
    return;
  }

  for (const key in keyValue) {
    const value = keyValue[key];

    if (isNullOrUndefined(value)) {
      continue;
    }

    if (typeof value === 'string') {
      appendFunc(key, value as string);
    } else if (typeof value === 'number' || typeof value === 'boolean') {
      appendFunc(key, '' + value);
    } else if (value instanceof Date) {
      appendFunc(key, '' + (value as Date).getTime());
    } else {
      throw new Error('Unknown type of parameter `' + key + '` : typeof value = `'
        + (typeof value) + '` : value = `' + value + '`');
    }

  }

};


@Injectable()
export class ApiService {

  constructor(private http: HttpClient) {
  }

  private prefix(): string {
    return environment.urlPrefix;
  }

  private url(endpoint: string): string {
    return `${this.prefix()}${endpoint}`;
  }

  /**
   * Обязательно нужно присвоить в локальный httpService объект который вернул этот метод
   * @param prefix префикс контроллера на сервере
   */
  public setPrefix(prefix: string): ApiService {
    const prefixHandler = {
      get(target: any, name, receiver) {
        if (name === 'url') {
          return (url) => {
            return target.__proto__.prefix() + prefix + url;
          };
        }
        return Reflect.get(target, name, receiver);
      }
    } as ProxyHandler<ApiService>;
    return new Proxy(this, prefixHandler);
  }

  get(endpoint: string, params?: any, reqOpts?: any): Observable<any> {
    const defReqOpts = {
      withCredentials: true,
      params: new HttpParams()
    };

    if (reqOpts) {
      for (const k in reqOpts) {
        defReqOpts[k] = reqOpts[k];
      }
    }

    // Support easy query params for GET requests
    if (params) {
      defReqOpts.params = new HttpParams();
      keyValueAppender(params, (key, value) => defReqOpts.params = defReqOpts.params.set(key, value));
    }

    return this.http.get(this.url(endpoint), defReqOpts);
  }

  delete(endpoint: string, params?: any, reqOpts?: any): Observable<any> {
    const defReqOpts = {
      withCredentials: true,
      params: new HttpParams()
    };

    if (reqOpts) {
      for (const k in reqOpts) {
        defReqOpts[k] = reqOpts[k];
      }
    }

    // Support easy query params for GET requests
    if (params) {
      defReqOpts.params = new HttpParams();
      keyValueAppender(params, (key, value) => defReqOpts.params = defReqOpts.params.set(key, value));
    }

    return this.http.delete(this.url(endpoint), defReqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    const defReqOpts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      withCredentials: true,
    };

    if (reqOpts) {
      for (const k in reqOpts) {
        defReqOpts[k] = reqOpts[k];
      }
    }

    return this.http.post(this.url(endpoint), body, defReqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    const defReqOpts = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      withCredentials: true,
    };

    if (reqOpts) {
      for (const k in reqOpts) {
        defReqOpts[k] = reqOpts[k];
      }
    }

    return this.http.put(this.url(endpoint), body, defReqOpts);
  }


}
