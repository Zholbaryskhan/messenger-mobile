import {Injector, ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {HttpRequest} from '@angular/common/http';

import {
  AUTH_FALLBACK_TOKEN,
  AuthService,
  AuthSimpleToken,
  AuthTokenClass,
  AuthTokenParceler,
  TokenLocalStorage,
  TokenService,
  TokenStorage,
} from './services';
import {
  AuthStrategy,
  AuthStrategyOptions,
  DummyAuthStrategy,
  OAuth2AuthStrategy,
  OAuth2JWTAuthStrategy,
  PasswordAuthStrategy,
} from './strategies';

import {
  AUTH_INTERCEPTOR_HEADER,
  AUTH_OPTIONS,
  AUTH_STRATEGIES,
  AUTH_TOKEN_INTERCEPTOR_FILTER,
  AUTH_TOKENS,
  AUTH_USER_OPTIONS,
  AuthOptions,
  AuthStrategyClass,
  defaultAuthOptions,
} from './auth.options';


import {deepExtend} from './helpers';

export function strategiesFactory(options: AuthOptions, injector: Injector): AuthStrategy[] {
  const strategies = [];
  options.strategies
    .forEach(([strategyClass, strategyOptions]: [AuthStrategyClass, AuthStrategyOptions]) => {
      const strategy: AuthStrategy = injector.get(strategyClass);
      strategy.setOptions(strategyOptions);

      strategies.push(strategy);
    });
  return strategies;
}

export function tokensFactory(strategies: AuthStrategy[]): AuthTokenClass[] {
  const tokens = [];
  strategies
    .forEach((strategy: AuthStrategy) => {
      tokens.push(strategy.getOption('token.class'));
    });
  return tokens;
}

export function optionsFactory(options) {
  return deepExtend(defaultAuthOptions, options);
}

export function noOpInterceptorFilter(req: HttpRequest<any>): boolean {
  return true;
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [],
  exports: [],
})
export class AuthModule {
  static forRoot(authOptions?: AuthOptions): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule,
      providers: [
        {provide: AUTH_USER_OPTIONS, useValue: authOptions},
        {provide: AUTH_OPTIONS, useFactory: optionsFactory, deps: [AUTH_USER_OPTIONS]},
        {provide: AUTH_STRATEGIES, useFactory: strategiesFactory, deps: [AUTH_OPTIONS, Injector]},
        {provide: AUTH_TOKENS, useFactory: tokensFactory, deps: [AUTH_STRATEGIES]},
        {provide: AUTH_FALLBACK_TOKEN, useValue: AuthSimpleToken},
        {provide: AUTH_INTERCEPTOR_HEADER, useValue: 'Authorization'},
        {provide: AUTH_TOKEN_INTERCEPTOR_FILTER, useValue: noOpInterceptorFilter},
        {provide: TokenStorage, useClass: TokenLocalStorage},
        AuthTokenParceler,
        AuthService,
        TokenService,
        DummyAuthStrategy,
        PasswordAuthStrategy,
        OAuth2AuthStrategy,
        OAuth2JWTAuthStrategy
      ],
    } as ModuleWithProviders<AuthModule>;
  }
}
