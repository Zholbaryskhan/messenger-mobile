import {AuthTokenClass} from '../services/';

export class AuthStrategyOptions {
  name: string;
  token?: {
    class?: AuthTokenClass;
    [key: string]: any;
  };
}
