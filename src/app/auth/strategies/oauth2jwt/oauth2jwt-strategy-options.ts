import {AuthJWTToken, AuthTokenClass} from '../../services';
import {AuthStrategyOptions} from '../auth-strategy-options';
import {getDeepFromObject} from '../../helpers';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

export interface OAuth2JWTStrategyModule {
  alwaysFail?: boolean;
  endpoint?: string;
  method?: string;
  redirect?: {
    success?: string | null;
    failure?: string | null;
  };
  requireValidToken?: boolean;
  defaultErrors?: string[];
  defaultMessages?: string[];
}

export interface OAuth2JWTStrategyReset extends OAuth2JWTStrategyModule {
  resetPasswordTokenKey?: string;
}

export interface OAuth2JWTStrategyToken {
  class?: AuthTokenClass,
  key?: string,
  getter?: Function,
}

export interface OAuth2JWTStrategyMessage {
  key?: string,
  getter?: Function,
}

export class OAuth2JWTAuthStrategyOptions extends AuthStrategyOptions {
  baseEndpoint? = '/api/auth/';
  login?: boolean | OAuth2JWTStrategyModule = {
    alwaysFail: false,
    endpoint: 'login',
    method: 'post',
    requireValidToken: false,
    redirect: {
      success: '/',
      failure: null,
    },
    defaultErrors: ['Login/Email combination is not correct, please try again.'],
    defaultMessages: ['You have been successfully logged in.'],
  };
  register?: boolean | OAuth2JWTStrategyModule = {
    alwaysFail: false,
    endpoint: 'register',
    method: 'post',
    requireValidToken: false,
    redirect: {
      success: '/',
      failure: null,
    },
    defaultErrors: ['Something went wrong, please try again.'],
    defaultMessages: ['You have been successfully registered.'],
  };
  requestPass?: boolean | OAuth2JWTStrategyModule = {
    endpoint: 'request-pass',
    method: 'post',
    redirect: {
      success: '/',
      failure: null,
    },
    defaultErrors: ['Something went wrong, please try again.'],
    defaultMessages: ['Reset password instructions have been sent to your email.'],
  };
  resetPass?: boolean | OAuth2JWTStrategyReset = {
    endpoint: 'reset-pass',
    method: 'put',
    redirect: {
      success: '/',
      failure: null,
    },
    resetPasswordTokenKey: 'reset_password_token',
    defaultErrors: ['Something went wrong, please try again.'],
    defaultMessages: ['Your password has been successfully changed.'],
  };
  logout?: boolean | OAuth2JWTStrategyReset = {
    alwaysFail: false,
    endpoint: 'logout',
    method: 'delete',
    redirect: {
      success: '/',
      failure: null,
    },
    defaultErrors: ['Something went wrong, please try again.'],
    defaultMessages: ['You have been successfully logged out.'],
  };
  refreshToken?: boolean | OAuth2JWTStrategyModule = {
    endpoint: 'refresh-token',
    method: 'post',
    requireValidToken: false,
    redirect: {
      success: null,
      failure: null,
    },
    defaultErrors: ['Something went wrong, please try again.'],
    defaultMessages: ['Your token has been successfully refreshed.'],
  };
  token?: OAuth2JWTStrategyToken = {
    class: AuthJWTToken,
    key: 'data.token',
    getter: (module: string, res: HttpResponse<Object>, options: OAuth2JWTAuthStrategyOptions) => getDeepFromObject(
      res.body,
      options.token.key,
    ),
  };
  errors?: OAuth2JWTStrategyMessage = {
    key: 'data.errors',
    getter: (module: string, res: HttpErrorResponse, options: OAuth2JWTAuthStrategyOptions) => getDeepFromObject(
      res.error,
      options.errors.key,
      options[module].defaultErrors,
    ),
  };
  messages?: OAuth2JWTStrategyMessage = {
    key: 'data.messages',
    getter: (module: string, res: HttpResponse<Object>, options: OAuth2JWTAuthStrategyOptions) => getDeepFromObject(
      res.body,
      options.messages.key,
      options[module].defaultMessages,
    ),
  };
  validation?: {
    password?: {
      required?: boolean;
      minLength?: number | null;
      maxLength?: number | null;
      regexp?: string | null;
    };
    email?: {
      required?: boolean;
      regexp?: string | null;
    };
    fullName?: {
      required?: boolean;
      minLength?: number | null;
      maxLength?: number | null;
      regexp?: string | null;
    };
  };
}

export const oauth2jwtStrategyOptions: OAuth2JWTAuthStrategyOptions = new OAuth2JWTAuthStrategyOptions();
