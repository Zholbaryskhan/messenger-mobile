import {Injectable} from '@angular/core';

import {AuthToken} from './token';
import {AuthTokenParceler} from './token-parceler';

export abstract class TokenStorage {

  abstract get(): AuthToken;

  abstract set(token: AuthToken);

  abstract clear();
}

/**
 * Service that uses browser localStorage as a storage.
 *
 * The token storage is provided into auth module the following way:
 * ```ts
 * { provide: TokenStorage, useClass: TokenLocalStorage },
 * ```
 *
 * If you need to change the storage behaviour or provide your own - just extend your class from basic `TokenStorage`
 * or `TokenLocalStorage` and provide in your `app.module`:
 * ```ts
 * { provide: TokenStorage, useClass: TokenCustomStorage },
 * ```
 *
 */
@Injectable()
export class TokenLocalStorage extends TokenStorage {

  protected key = 'auth_app_token';

  constructor(private parceler: AuthTokenParceler) {
    super();
  }

  /**
   * Returns token from localStorage
   * @returns {AuthToken}
   */
  get(): AuthToken {
    const raw = localStorage.getItem(this.key);
    return this.parceler.unwrap(raw);
  }

  /**
   * Sets token to localStorage
   * @param {AuthToken} token
   */
  set(token: AuthToken) {
    const raw = this.parceler.wrap(token);
    localStorage.setItem(this.key, raw);
  }

  /**
   * Clears token from localStorage
   */
  clear() {
    localStorage.removeItem(this.key);
  }
}
