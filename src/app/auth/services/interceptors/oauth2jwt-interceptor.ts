import {Inject, Injectable, Injector} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {AUTH_TOKEN_INTERCEPTOR_FILTER} from '../../auth.options';
import {BehaviorSubject, Observable, throwError} from "rxjs";
import {catchError, filter, switchMap, take} from "rxjs/operators";
import {AuthOAuth2Token} from "@auth/services";
import {isNotNullOrUndefined} from "codelyzer/util/isNotNullOrUndefined";

@Injectable()
export class OAuth2JWTInterceptor implements HttpInterceptor {

  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private injector: Injector,
              @Inject(AUTH_TOKEN_INTERCEPTOR_FILTER) protected filter) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.filter(req)) return next.handle(req);

    return this.authService.getToken().pipe(
      switchMap((token: AuthOAuth2Token) => {
        // if (token && token.getValue() && token.isValid()) {
        if (token && token.getValue()) {
          // if (token && token.getValue()) {
          req = this.addToken(req, token.getValue());
        }

        return next.handle(req);
      }),
      catchError(error => {
        if (error instanceof HttpErrorResponse
          && error.status === 401
          && error.error.errorCode === 11) {

          return this.handle401ErrorCode11(req, next, error);
        }

        return throwError(error);
      })
    );
  }

  private addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    const JWT = `Bearer ${token}`;

    return req.clone({
      setHeaders: {
        Authorization: JWT,
      },
    });
  }

  private handle401ErrorCode11(request: HttpRequest<any>, next: HttpHandler, error: HttpErrorResponse): Observable<HttpEvent<any>> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);

      return this.authService.isAuthenticatedOrRefresh().pipe(
        switchMap((authenticated) => {
          this.isRefreshing = false;

          if (!authenticated) {
            // Request is sent to server without authentication so that the client code
            // receives the 401/403 error and can act as desired ('session expired', redirect to login, aso)
            // TODO fix it asset
            // return throwError({error: {errorCode: 10}, status: 401});//next.handle(request);
            return throwError(error);
          }

          return this.authService.getToken().pipe(
            switchMap((token: AuthOAuth2Token) => {

              this.refreshTokenSubject.next(token.getValue());

              return next.handle(this.addToken(request, token.getValue()));
            }),
          );
        }));

    } else {
      return this.refreshTokenSubject.pipe(
        filter(token => isNotNullOrUndefined(token)),
        take(1),
        switchMap(token => {
          return next.handle(this.addToken(request, token));
        }));
    }
  }

  protected get authService(): AuthService {
    return this.injector.get(AuthService);
  }
}
