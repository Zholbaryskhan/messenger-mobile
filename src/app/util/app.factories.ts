import {environment} from '../../environments/environment';
import {AppLoaderService} from '../services/app-loader.service';
import {HttpRequest} from '@angular/common/http';

export function filterInterceptorRequest(req: HttpRequest<any>) {
  return [
    `${environment.urlPrefix}/auth/login`,
    `${environment.urlPrefix}/auth/refresh-token`
  ]
    .some(url => req.url.includes(url));
}

export function initApp(appLoaderService: AppLoaderService) {
  return () => appLoaderService.initApp();
}
