import {isNumber} from 'util';

export class EnumHelpers {
  // static getSimpleRecords(e: any) {
  //   return EnumHelpers.getValues(e).map(n => ({id: n, title: e[n] as string})) as SimpleRecord[];
  // }

  static getValues(e: any) {
    return EnumHelpers.getObjValues(e) as string[];
  }

  static getValue(e: any, k:any) {
    return EnumHelpers.getValues(e).find(v => e[v] === k);
  }

  private static getObjValues(e: any): (number | string)[] {
    return Object.keys(e).filter(v => typeof e[v] !== "function" && (isNumber(v) || v !== e[e[v]]));
  }
}
