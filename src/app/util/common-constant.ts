export const QAMQOR_DID_TUTORIAL = 'qamqor_did_tutorial';

export const EMAIL_PATTERN = '^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$';
export const PHONE_PATTERN = '\\(?[\\d]{3}\\)?[\\d]{3}-?[\\d]{2}-?[\\d]{2}';
export const PHONE_MASK = '(000)-000-00-00';
export const PHONE_PREFIX = '+7 ';

export const CAPTCHA_LANG = 'ru';
