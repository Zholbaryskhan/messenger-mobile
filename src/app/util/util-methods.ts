import {Platform} from '@ionic/angular';

export function isMobile(platform: Platform): boolean {
  return !platform.is('mobileweb') && !platform.is('desktop');
}

export function filterToKeyValue(filter: any): any {
  const ret = {}
  for (const i in filter) {
    ret[i] = filter[i];
  }

  return ret;
}
