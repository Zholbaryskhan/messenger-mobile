import {environment} from '../../environments/environment';
import {EMAIL_PATTERN, PHONE_PATTERN} from '@util/common-constant';
import {AuthOAuth2JWTToken, AuthOptions, OAuth2JWTAuthStrategy} from '@auth/index';

export const authOptions: AuthOptions = {

  strategies: [
    OAuth2JWTAuthStrategy.setup({
      name: 'email',
      baseEndpoint: `${environment.urlPrefix}/auth/`,
      login: {
        requireValidToken: true
      },
      token: {
        class: AuthOAuth2JWTToken
      },
      errors: {
        key: 'message'
      }
    })
  ],

  forms: {
    login: {
      redirectDelay: 0
    },
    validation: {
      password: {
        pattern: null,
        minLength: 2,
      },
      name: {
        required: true,
        minLength: 2,
        maxLength: 50,
      },
      email: {
        pattern: EMAIL_PATTERN,
      },
      phone: {
        required: true,
        pattern: PHONE_PATTERN,
      },
      license: {
        required: true,
      },
      reCaptcha: {
        required: true,
      }
    }
  }
};
