import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {TestService} from '../../services/test.service';

export interface Friends {
  id: string;
  fio: string;
  city: string;
  src: string;
}

@Component({
  selector: 'app-my-friends',
  templateUrl: './my-friends.page.html',
  styleUrls: ['./my-friends.page.scss'],
})
export class MyFriendsPage implements OnInit {

  friends: Friends[] = [{id: 'dfsafdsfa', fio: 'Шорахан Жолбарысхан', city: 'Almaty', src: 'assets/icon/aza.jpg'},
    {id: 'dfsfbghfshtres', fio: 'Ербосын Азамат', city: 'Almaty', src: 'assets/icon/aza.jpg'}]

  constructor(private router: Router,
              private testService: TestService,
              private userService: UserService) { }

  ngOnInit() {
  }

  clickDetail(id: string) {
    this.router.navigate(['app/my-friend/person-detail', id]);
  }

  testController() {
    this.testService.list().then(res => console.log(res));
  }
}
