import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {MyFriendsPageRoutingModule} from './my-friends-routing.module';

import {MyFriendsPage} from './my-friends.page';
import {FriendModule} from '../../components/friend/friend.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyFriendsPageRoutingModule,
    FriendModule,
  ],
  declarations: [MyFriendsPage]
})
export class MyFriendsPageModule {}
