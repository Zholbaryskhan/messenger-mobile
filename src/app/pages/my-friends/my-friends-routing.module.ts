import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyFriendsPage } from './my-friends.page';

const routes: Routes = [
  {
    path: '',
    component: MyFriendsPage
  },
  {
    path: 'person-detail/:id',
    loadChildren: '@pages/person-detail/person-detail.module#PersonDetailPageModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyFriendsPageRoutingModule {}
