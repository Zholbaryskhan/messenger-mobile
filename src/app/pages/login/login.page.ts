import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AUTH_OPTIONS, AuthResult, AuthService, AuthSocialLink} from '@auth/index';
import {Router} from '@angular/router';
import {getDeepFromObject} from '@auth/helpers';
import {NavController} from "@ionic/angular";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginPage implements OnInit {

  form: FormGroup;

  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  socialLinks: AuthSocialLink[] = [];
  rememberMe = false;

  errors: string[] = [];
  messages: string[] = [];

  submitted: boolean = false;

  constructor(private fb: FormBuilder,
              private service: AuthService,
              @Inject(AUTH_OPTIONS) protected options = {},
              protected cd: ChangeDetectorRef,
              private navCtrl: NavController,
              private router: Router) {

    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');
    this.strategy = this.getConfigValue('forms.login.strategy');
    this.socialLinks = this.getConfigValue('forms.login.socialLinks');
  }

  ngOnInit() {
    this.rememberMe = this.getConfigValue('forms.login.rememberMe');
    const usernameValidations = [];
    const passwordValidations = [
      Validators.minLength(this.getConfigValue('forms.validation.password.minLength')),
      Validators.maxLength(this.getConfigValue('forms.validation.password.maxLength')),
      Validators.pattern(this.getConfigValue('forms.validation.password.pattern'))
    ];

    if (this.getConfigValue('forms.validation.email.required')) {
      usernameValidations.push(Validators.required);
    }

    if (this.getConfigValue('forms.validation.password.required')) {
      passwordValidations.push(Validators.required);
    }

    this.form = this.fb.group({
      username: ['', usernameValidations],
      password: ['', passwordValidations],
      rememberMe: [this.rememberMe]
    });
  }

  login() {
    const credential = this.form.getRawValue();

    this.errors = [];
    this.messages = [];
    this.submitted = true;

    console.log('form:', credential);

    this.service.authenticate(this.strategy, credential).subscribe((result: AuthResult) => {
      this.submitted = false;

      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {
        this.errors = result.getErrors();
      }

      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.navCtrl.navigateRoot(redirect);
        }, this.redirectDelay);
      }
      this.cd.detectChanges();
    });

  }

  forgotPassword() {
    console.log('forgotPassword');
  }

  registration() {
    this.router.navigate(['/login/registration']);
  }

  G;

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
