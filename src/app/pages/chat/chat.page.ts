import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OutputMessage} from '../../model/chat/OutputMessage';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import {AuthService} from '@auth/services';
import {ChatService} from '../../services/chat.service';
import {ChatDetail} from '../../model/chat/chat-detail';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  defaultHref: string = '/app/chat-list';

  serverUrl: string = 'http://'+location.hostname+':8080/ws';
  private stompClient;

  id: number;
  messages: OutputMessage[] = [];
  chatDetail: ChatDetail = {} as ChatDetail;

  constructor(private route: ActivatedRoute,
              private chatService: ChatService,
              private authService: AuthService) { }

  ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));



    this.chatService.getChatDetail(this.id).then(res => {
      this.chatDetail = res;
      this.initializeWebSocketConnection();
      this.loadMessages();
    });
  }


  loadMessages(){
    this.chatService.listMessage('offer' + this.chatDetail.channelId).then(res => {
      this.messages = res;
    })
  }

  onSendMessage(value: string | number) {
    const msg = {
      // 'from': '' + this.username,
      channelId: 'offer' + this.chatDetail.channelId,
      text: value,
      to: this.chatDetail.id
    };
    this.stompClient.send('/web/secured/room' , {}, JSON.stringify(msg));
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;

    let token = '';
    this.authService.getToken().subscribe(value => token = value.getValue());
    this.stompClient.connect({'X-Authorization': 'Bearer ' + token, Platform: 'MOBILE'}, function(frame) {
      that.stompClient.subscribe('/secured/user/queue/specific-user' + '-offer' + that.chatDetail.channelId, (message) => {
        if (message.body) {
          const msgBody = JSON.parse(message.body);
          const msg: OutputMessage = msgBody as OutputMessage;
          msg.own = msgBody.from === frame.headers['user-name'];
          msg.avatar = 'https://sun7-9.userapi.com/c857120/v857120101/107447/amSAhsnHBLM.jpg';
          that.messages.push(msg);
        }
      });

    });

  }
}
