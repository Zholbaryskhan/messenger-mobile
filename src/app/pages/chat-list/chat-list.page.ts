import { Component, OnInit } from '@angular/core';
import {ChatList} from '../../model/chat/chat-list';
import {Router} from '@angular/router';
import {ChatService} from '../../services/chat.service';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.page.html',
  styleUrls: ['./chat-list.page.scss'],
})
export class ChatListPage implements OnInit {

  chatList: ChatList[];

  constructor(private router: Router,
              private chatService: ChatService) { }

  ngOnInit() {

    this.loadChatList();

  }
  loadChatList(){
    this.chatService.getChatList().then(res => this.chatList = res);
  }

  onClickChat(id: number) {
    this.router.navigate(['/app/chat-list/chat-message', id]);
  }
}
